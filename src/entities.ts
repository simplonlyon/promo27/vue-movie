export interface Movie {
    title:string;
    image:string;
    description: string;

}

export interface Serie {
    name:string;
}

export interface Actor {
    nom : string;
    prenom : string;
    photo : string ;
}