
import Serie from "@/views/Serie.vue";
import Home from "@/views/Home.vue";
import { createRouter, createWebHistory } from "vue-router";
import Actor from "@/views/Actor.vue";

export const router  = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    // linkActiveClass: 'active',
    routes: [
        {path: '/', component: Home},
        {path: '/series', component: Serie},
        {path: '/acteurs', component: Actor},
      
    ]
});
